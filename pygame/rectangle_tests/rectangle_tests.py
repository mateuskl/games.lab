from pygame import Rect

def testCollide(r, s, i):
    print ('=' * 40)
    print 'Test: ' + str(i)
    print 'r: ' + str(r)
    print 's: ' + str(s)
    print 'r.colliderect(s)? ', r.colliderect(s)
    print 'r.right: ' + str(r.right)
    print 's.right: ' + str(s.right)
    return i + 1

def testCollideWithPoint(r, p):
    print ('~' * 40)
    print 'r: ' + str(r)
    print 'p: ' + str(p)
    print 'r.collidepoint(p)? ', r.collidepoint(p)
    print 'r.bottomleft ', r.bottomleft
    print 'r.bottomright ', r.bottomright
    print 'r.topright ', r.topright
    
def main():
    i = 0

    r = Rect(0, 0, 5, 5)
    s = Rect(0, 0, 5, 5)
    i = testCollide(r, s, i)

    # ----------------------
    r = Rect(0, 0, 5, 5)
    s = Rect(5, 0, 5, 5)
    i = testCollide(r, s, i)

    r = Rect(5, 0, 5, 5)
    s = Rect(0, 0, 5, 5)
    i = testCollide(r, s, i)
    # ----------------------

    r = Rect(0, 0, 5, 5)
    s = Rect(0, 5, 5, 5)
    i = testCollide(r, s, i)

    r = Rect(0, 0, 5, 5)
    s = Rect(5, 0, 5, 5)
    i = testCollide(r, s, i)

    print 'point tests'
    r = Rect(10, 10, 5, 5)
    p = (10, 10)
    testCollideWithPoint(r, p)

    r = Rect(10, 10, 5, 5)
    p = (10, 15)
    testCollideWithPoint(r, p)        

    r = Rect(10, 10, 5, 5)
    p = (10, 14)
    testCollideWithPoint(r, p)        

    r = Rect(10, 10, 5, 5)
    p = (14, 10)
    testCollideWithPoint(r, p)        

    r = Rect(10, 10, 5, 5)
    p = (15, 10)
    testCollideWithPoint(r, p)        


if __name__ == '__main__':
    main()


