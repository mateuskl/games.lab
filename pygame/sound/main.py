#coding: utf8

import os

import pygame
from pygame.locals import *


def main():
    pygame.mixer.init()
    squareman_in = pygame.mixer.Sound(os.path.join('res', 'sound', 'squareman_in.wav'))
    squareman_out = pygame.mixer.Sound(os.path.join('res', 'sound', 'squareman_out.wav'))
    skiessi = pygame.mixer.Sound(os.path.join('res', 'music', 'skiessi-c64.wav'))
    print 'hello'
    while True:
        command = str(raw_input('command: q (quit), i (play squareman_in), o (play squareman_out), j (play skiessi), k (stop skiessi)\n'))
        
        if command == 'q':
            break
        elif command == 'i':
            squareman_in.play()
        elif command == 'o':
            squareman_out.play()
        elif command == 'j':
            skiessi.play()
        elif command == 'k':
            skiessi.stop()

    print 'bye'


if __name__ == '__main__':
    main()
    
