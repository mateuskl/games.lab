#coding: utf8

import os

import pygame
from pygame.locals import *


def main():
    pygame.mixer.init()
    pygame.mixer.music.load(os.path.join('res', 'mozart_kv525.wav'))
    pygame.mixer.music.play()
    print 'hello'
    while True:
        command = str(raw_input('command: q (quit), a (play again), p (pause), r (resume), f (fade-out), s (stop)\n'))
        
        if command == 'q':            
            break
        elif command == 'a':
            pygame.mixer.music.play()
        elif command == 'p':
            pygame.mixer.music.pause()
        elif command == 'r':
            pygame.mixer.music.unpause()
        elif command == 'f':
            pygame.mixer.music.fadeout(2000)
        elif command == 's':
            pygame.mixer.music.stop()

if __name__ == '__main__':
    main()
    
