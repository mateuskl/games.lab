#include <allegro.h>

#include <iostream>

using namespace std;

int main()
{
    cout << "hello\n";

    allegro_init();
        
    int soundInstallError = install_sound(DIGI_AUTODETECT, MIDI_NONE, 0);
    if (soundInstallError)
    {
        cout << "Error initializing sound system" << endl;
        cout << allegro_error << endl;
        return -1;
    }
    
    SAMPLE* sample = load_sample("res/mozart_kv525.wav");
    if (!sample) 
    {
        allegro_message("Error reading wave file");
        return -1;
    }
    

    int voice = allocate_voice(sample);   
    voice_start(voice);
    
    
    cout << "bye\n";

    while (true)
    {    
        char command;
        cout << "command: q (quit), a (play again), p (pause), r (resume), f (fade-out), s (stop)\n" << endl;
        cin >> command ;
        
        if (command == 'q') {
            break;
        }
        else if (command == 'a') {
            // pygame.mixer.music.play()
            // OK. equals to pygame. Calling it while playing, restarts the music.
            voice_stop(voice);
            voice_set_position(voice, -1);
            voice_start(voice);
        }
        else if (command == 'p') {            
            // pygame.mixer.music.pause()
            // OK. equals to pygame.
            voice_stop(voice);
        }
        else if (command == 'r') {
            // pygame.mixer.music.unpause()
            // NOT OK. NOT equals to pygame. Resume cannot start a stopped music.
            voice_start(voice);
        }
        else if (command == 'f') {
            // pygame.mixer.music.fadeout(2000)
        }
        else if (command == 's') {
            // pygame.mixer.music.stop()
            // OK. equals to pygame.
            voice_stop(voice);
            voice_set_position(voice, -1);            
        }
    }

    deallocate_voice(voice);
    destroy_sample(sample);

    return 0;
}

END_OF_MAIN()
