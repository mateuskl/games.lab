#include <allegro.h>

#include <iostream>

using namespace std;

int main()
{
    cout << "hello\n";

    allegro_init();
    // cout << "allegro initiated\n";    
    
    int soundInstallError = install_sound(DIGI_AUTODETECT, MIDI_NONE, 0);
    if (soundInstallError)
    {
        allegro_message("Error initializing sound system");
        return -1;
    }
    // cout << "sound installed\n";

    SAMPLE* sample = load_sample("res/mozart_kv525.wav");
    if (!sample) 
    {
        allegro_message("Error reading wave file");
        return -1;
    }
    // cout << "sample loaded\n";

    
    int panning = 128;
    int pitch = 1000;
    int volume = 255;
    play_sample(sample, volume, panning, pitch, 0);
    cout << "playing audio sample\n";
    
    cout << "bye\n";

    while (true)
    {    
        char command;
        cout << "command: q (quit), a (play again), p (pause), r (resume), f (fade-out), s (stop)\n" << endl;
        cin >> command ;
        
        if (command == 'q') {
            break;
        }
        else if (command == 'a') {
            // pygame.mixer.music.play()
            play_sample(sample, volume, panning, pitch, 0);
        }
        else if (command == 'p') {            
            // pygame.mixer.music.pause()
            //voice_stop(sample->voice);
        }
        else if (command == 'r') {
            // pygame.mixer.music.unpause()
            //voice_start(sample->voice);
        }
        else if (command == 'f') {
            // pygame.mixer.music.fadeout(2000)
        }
        else if (command == 's') {
            // pygame.mixer.music.stop()
            stop_sample(sample);
        }
    }

    destroy_sample(sample);

    return 0;
}

END_OF_MAIN()
