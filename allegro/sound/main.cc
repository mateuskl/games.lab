#include <allegro.h>
#include <iostream>

using namespace std;

int main()
{
    cout << "hello\n";

    allegro_init();

    if (install_sound(DIGI_AUTODETECT, MIDI_NONE, 0))
    {
        cout << "Error initializing sound system" << endl;
        cout << allegro_error << endl;
        return -1;
    }

    SAMPLE* squareman_in = load_sample("res/sound/squareman_in.wav");
    if (! squareman_in)
    {
        allegro_message("Error reading wave file: squareman_in.wav");
        return -1;
    }    

    SAMPLE* squareman_out = load_sample("res/sound/squareman_out.wav");
    if (! squareman_out)
    {
        allegro_message("Error reading wave file: squareman_out.wav");
        return -1;
    }

    SAMPLE* skiessi = load_sample("res/music/skiessi-c64.wav");
    if (! skiessi)
    {
        allegro_message("Error reading wave file: skiessi-64.wav");
        return -1;
    }


    int panning = 128;
    int pitch = 1000;
    int volume = 255;

    while (true)
    {
        char command;
        cout << "command: q (quit), i (play squareman_in), o (play squareman_out), j (play skiessi), k (stop skiessi)\n";
        cin >> command;
        
        if (command == 'q')
        {
            break;
        }
        else if (command == 'i')
        {
            play_sample(squareman_in, volume, panning, pitch, 0);
        }
        else if (command == 'o')
        {
            play_sample(squareman_out, volume, panning, pitch, 0);
        }
        else if (command == 'j')
        {
            play_sample(skiessi, volume, panning, pitch, 0);
        }
        else if (command == 'k')
        {
            stop_sample(skiessi);
        }
    }

    destroy_sample(squareman_in);
    destroy_sample(squareman_out);
    destroy_sample(skiessi);

    cout << "bye\n";

    return 0;
}

END_OF_MAIN()
